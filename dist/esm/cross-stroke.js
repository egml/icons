var crossStroke = "<symbol id=\"egml-icons-cross-stroke\" viewBox=\"0 0 20 20\"><path d=\"M 16.589922,2.0000143 10,8.5890049 3.4120547,2.0019908 1.9999453,3.4120092 8.5898438,10.000976 1.9999219,16.587991 3.4100781,17.998009 10,11.410995 l 6.589922,6.589014 1.410156,-1.410018 -6.589922,-6.589015 6.589922,-6.5889903 z\" clip-rule=\"evenodd\" fill-rule=\"evenodd\"/></symbol>";

export default crossStroke;
