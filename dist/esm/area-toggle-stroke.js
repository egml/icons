var areaToggleStroke = "<symbol id=\"egml-icons-area-toggle-stroke\" viewBox=\"0 0 20 20\"><path d=\"M 1.411,19.999 0,18.588 l 2.044,-2.044 0,-14.549 14.55,0 L 18.59,0 20,1.411 Z M 4.04,3.99 4.04,14.549 14.6,3.99 Z m 7.981,13.964 -3.992,0 0,-1.994 3.991,0 0,1.994 z m 3.989,-9.974 1.995,0 0,3.99 -1.995,0 z m 0,5.985 1.995,0 0,3.989 -3.989,0 0,-1.994 1.994,0 z\" clip-rule=\"evenodd\" fill-rule=\"evenodd\"/></symbol>";

export default areaToggleStroke;
